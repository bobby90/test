package com.thinksoft.fun.entity.bean.circle;

import com.thinksoft.fun.entity.BaseBean;

import java.util.List;

/**
 * @author txf
 * @create 2019/3/28 0028
 * @
 */
public class RegionsBean extends BaseBean {
    List<RegionsDataBean> regions;

    public void setRegions(List<RegionsDataBean> regions) {
        this.regions = regions;
    }

    public List<RegionsDataBean> getRegions() {
        return regions;
    }
}
