package com.thinksoft.fun.entity.bean.img;

import com.thinksoft.fun.entity.BaseBean;

/**
 * @author txf
 * @create 2019/3/23 0023
 * @
 */
public class ImagInfoBean extends BaseBean {
    ImagInfoDataBean pictureInfo;

    public void setPictureInfo(ImagInfoDataBean pictureInfo) {
        this.pictureInfo = pictureInfo;
    }

    public ImagInfoDataBean getPictureInfo() {
        return pictureInfo;
    }
}
