package com.thinksoft.fun.entity.bean;


import com.thinksoft.fun.entity.BaseBean;

/**
 * @author txf
 * @create 2018/12/17 0017
 * @
 */
public class UserBean extends BaseBean {
    String token;

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
