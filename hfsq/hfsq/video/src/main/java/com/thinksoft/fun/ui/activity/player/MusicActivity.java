package com.thinksoft.fun.ui.activity.player;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.tencent.rtmp.TXVodPlayer;
import com.tencent.rtmp.ui.TXCloudVideoView;
import com.thinksoft.fun.R;
import com.thinksoft.fun.entity.bean.music.MusicInfoDataBean;
import com.thinksoft.fun.ui.view.player.tx.MusicControlView;
import com.txf.ui_mvplibrary.ui.activity.BaseActivity;

/**
 * @author txf
 * @create 2019/3/23 0023
 * @有声播放页
 */
public class MusicActivity extends BaseActivity {

    TXCloudVideoView mTXCloudVideoView;
    MusicControlView mMusicControlView;
    TXVodPlayer mVodPlayer;

    MusicInfoDataBean mMusicInfoDataBean;

    public static Intent getIntent(Context context, MusicInfoDataBean videoBean) {
        Intent i = new Intent(context, MusicActivity.class);
        i.putExtra("data", videoBean);
        return i;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_music;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        initData();
    }

    private void initData() {
        mMusicInfoDataBean = (MusicInfoDataBean) getIntent().getSerializableExtra("data");
        //创建 player 对象
        mVodPlayer = new TXVodPlayer(getContext());
        //关键 player 对象与界面 view
        mVodPlayer.setPlayerView(mTXCloudVideoView);
        mMusicControlView.setData(mMusicInfoDataBean, mVodPlayer);
        mMusicControlView.start();

    }

    @Override
    protected void onDestroy() {
        if (mVodPlayer != null)
            mVodPlayer.stopPlay(true);
        if (mTXCloudVideoView != null)
            mTXCloudVideoView.onDestroy();
        super.onDestroy();
    }

    private void initView() {
        mTXCloudVideoView = findViewById(R.id.TXCloudVideoView);
        mMusicControlView = findViewById(R.id.mMusicControlView);
        findViewById(R.id.backButton2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
