package com.thinksoft.fun.ui.fragment;

import com.thinksoft.fun.R;
import com.txf.ui_mvplibrary.ui.fragment.BaseMvpLjzFragment;

/**
 * @author txf
 * @create 2019/3/21 0021
 * @
 */
public class TestFragment extends BaseMvpLjzFragment {
    @Override
    protected int getLayoutId() {
        return R.layout.layout_text_developing;
    }

    @Override
    protected void request() {

    }
}
