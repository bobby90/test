package com.thinksoft.fun.entity.bean.video;

import com.thinksoft.fun.entity.BaseBean;
import com.thinksoft.fun.entity.bean.home.CatesBean;

import java.util.ArrayList;

/**
 * @author txf
 * @create 2019/3/11 0011
 * @
 */
public class TypeBean extends BaseBean {
    ArrayList<CatesBean> cates;

    public ArrayList<CatesBean> getCates() {
        return cates;
    }
    public void setCates(ArrayList<CatesBean> cates) {
        this.cates = cates;
    }
}
