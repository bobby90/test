package com.thinksoft.fun.entity.bean.video;

import com.thinksoft.fun.entity.bean.home.VideosBean;

import java.util.ArrayList;

/**
 * @author txf
 * @create 2019/3/11 0011
 * @
 */
public class TypeDataBean {
    ArrayList<VideosBean> vdieos;

    public ArrayList<VideosBean> getVdieos() {
        return vdieos;
    }

    public void setVdieos(ArrayList<VideosBean> vdieos) {
        this.vdieos = vdieos;
    }
}
