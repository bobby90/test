package com.thinksoft.fun.entity.bean;

import com.thinksoft.fun.entity.BaseBean;

import java.util.List;

/**
 * @author txf
 * @create 2019/3/1 0001
 * @
 */
public class RechargeBean extends BaseBean {

    private List<RechargeListBean> rechargeList;

    public List<RechargeListBean> getRechargeList() {
        return rechargeList;
    }

    public void setRechargeList(List<RechargeListBean> rechargeList) {
        this.rechargeList = rechargeList;
    }


}
